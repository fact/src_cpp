// STDLIB:
#include <cstdio>
#include <iostream>
#include <fstream>
#include "string.h"
#include <cstdlib>

// MINE
#include "vec.h"
#include "mat.h"
#include "lmat.h"
#include "dynprog.h"
#include "pngio.h"

#define DEBUG (0)

bool format_is_labelless(const char *format);

static const char *getFilenameExt(const char *filename) {
    const char *dot = strrchr(filename, '.');
    if (!dot || dot == filename) return NULL;
    return dot + 1;
}
#ifdef __cplusplus
extern "C" {
#endif
#ifdef _MSC_VER
__declspec(dllexport)
#endif
int obiwarp(
   char *file1, char *file2,
   char *format,
   char *outfile,
   int images,
   char *timefile,
   char *score,
   int local,
   int nostdnrm,
   float factor_diag, float factor_gap,
   float gap_init, float gap_extend,
   float init_penalty,
   float response,
   char *smat_in, char* smat_out) {

    // ************************************************************
    // * READ IN FILES TO GET MAT
    // ************************************************************
    LMat lmat1;
    LMat lmat2;
    MatF smat;
    DynProg dyn;

    const char *ext1 = getFilenameExt(file1);
    const char *ext2 = getFilenameExt(file2);

    if (((format) && (!strcmp(format, "mat"))) || ((!format) && (!strcmp(ext1, "mat")) && (!strcmp(ext2, "mat")))) {
        lmat1.set_from_binary_mat(file1);
        lmat2.set_from_binary_mat(file2);
    }
    else if (((format) && (!strcmp(format, "mata"))) || ((!format) && (!strcmp(ext1, "mata")) && (!strcmp(ext2, "mata")))) {
        lmat1.set_from_ascii_mat(file1);
        lmat2.set_from_ascii_mat(file2);
    }
    else if (((format) && (!strcmp(format, "lmat"))) || ((!format) && (!strcmp(ext1, "lmat")) && (!strcmp(ext2, "lmat")))) {
        lmat1.set_from_binary(file1);
        lmat2.set_from_binary(file2);
    }
    else if (((format) && (!strcmp(format, "lmata"))) || ((!format) && (!strcmp(ext1, "lmata")) && (!strcmp(ext2, "lmata")))) {
        lmat1.set_from_ascii(file1);
        lmat2.set_from_ascii(file2);
    }
    else {
        return -1;
    }

    bool outfile_is_stdout = 0;
    if (outfile != NULL) {
        if (!strcmp(outfile, "STDOUT")) {
            outfile_is_stdout = 1;
        }
    }

    ////puts("LMAT1 AND LMAT2"); lmat1.print(); lmat2.print();

    // ************************************************************
    // * SCORE THE MATRICES
    // ************************************************************
    if (DEBUG) {
        std::cerr << "Scoring the mats!\n";
    }
    if (smat_in != NULL) {
        smat.set_from_binary(smat_in);
        dyn._smat = &smat;
    }
    else {
        dyn.score(*(lmat1.mat()), *(lmat2.mat()), smat, score);
        // SETTING THE SMAT TO BE std normal
        if (!nostdnrm) {
            if (!smat.all_equal()) {
                smat.std_normal();
            }
        }
        if (!strcmp(score, "euc")) {
            smat *= -1; // inverting euclidean
        }
    }
    if (smat_out != NULL) {
        std::cerr << "Writing binary smat to '" << smat_out << "'\n";
        smat.write(smat_out);
        //smat.print(smat_out_files[0]);
        return 0;
    }

    // ************************************************************
    // * PREPARE GAP PENALTY ARRAY
    // ************************************************************

    MatF time_tester;
    MatF time_tester_trans;
    VecF mpt;
    VecF npt;
    VecF mOut_tm;
    VecF nOut_tm;

    int gp_length = smat.rows() + smat.cols();

    VecF gp_array;
    dyn.linear_less_before(gap_extend, gap_init,gp_length, gp_array);

    // ************************************************************
    // * DYNAMIC PROGRAM
    // ************************************************************
    int minimize = 0;
    if (DEBUG) {
        std::cerr << "Dynamic Time Warping Score Matrix!\n";
    }
    dyn.find_path(smat, gp_array, minimize, factor_diag, factor_gap, local, init_penalty);

    VecI mOut;
    VecI nOut;
    dyn.warp_map(mOut, nOut, response, minimize);
    //puts("mOUT"); mOut.print(); nOut.print();

    // Major output unless its the only case where we don't need warped time
    // values
    if (!(outfile_is_stdout && format_is_labelless(format))) {
        // MAJOR OUTPUT:
        VecF nOutF;
        VecF mOutF;
        lmat1.tm_axis_vals(mOut, mOutF);
        lmat2.tm_axis_vals(nOut, nOutF); //
        lmat2.warp_tm(nOutF, mOutF);
        lmat2.tm()->print(1);
    }

    // No labels on matrix and we have an outfile to produce
    // Needs to be after MAJOR OUTPUT since it warps the data!
    if ((format) && (format_is_labelless(format) && outfile)) {
        // @TODO: implement data warping here
    }

    // All subroutines below should write to the specified file
    // if the file == NULL then they should write to stdout!
    // outfile is set to NULL if "STDOUT" is specified!
    if ((outfile) && (format)) {
        if (!strcmp(format, "mat")) {
            lmat2.mat()->write(outfile);
        }
        else if (!strcmp(format, "mata")) {
            lmat2.mat()->print(outfile);
        }
        else if (!strcmp(format, "lmat")) {
            lmat2.write(outfile);
        }
        else if (!strcmp(format, "lmata")) {
            lmat2.print(outfile);
        }
        else {
            std::cerr << "Can't output to" << format << "format (yet)\n";
            return -1;
        }
    }

    // After all other output to stdout
    if (timefile != NULL) {
        time_tester.set_from_ascii(timefile, 1);  // no headers on the files
        time_tester.transpose(time_tester_trans);
        mpt.set(time_tester_trans.cols(), time_tester_trans.pointer(0));
        npt.set(time_tester_trans.cols(), time_tester_trans.pointer(1));
        float ssr, asr, sad, aad;
        dyn.path_accuracy((*lmat1._tm), (*lmat2._tm), mOut, nOut, mpt, npt, ssr, asr, sad, aad);
        printf("%f %f %f %f\n", ssr, asr, sad, aad);
    }


    if (images) {
        PngIO wrt(1);
        char base_fn[1024];
        strcpy(base_fn, "obi-warp_");
        char tb_fn[1024];
        strcpy(tb_fn, base_fn);
        strcat(tb_fn, "tb.png");
        //char *tb_fn = "tb.png";
        wrt.write(tb_fn, dyn._tb);
        char tbpath_fn[1024];
        strcpy(tbpath_fn, base_fn);
        strcat(tbpath_fn, "tbpath.png");
        wrt.write(tbpath_fn, dyn._tbpath);

        char asmat_fn[1024];
        strcpy(asmat_fn, base_fn);
        strcat(asmat_fn, "asmat.png");
        //wrt.write(asmat_fn, dyn._asmat);

        //strcpy(base_fn, "tb.png");
        //char *tbpath_fn = "tbpath.png";
        //char *tbscores_fn = "tbscores.png";
        //wrt.write(tbscores_fn, dyn._tbscores);
        //char *asmat_fn = "asmat.png";
        //wrt.write(asmat_fn, dyn._asmat);
        char *smat_fn = (char *)"smat.png";
        //wrt.write(smat_fn, *dyn._smat);
    }

/*
   char silly[100];
   strcpy(silly, "png_");
   char tmpp[5];
   sprintf(tmpp, "%d", i);
   strcat(silly, tmpp);
   strcat(silly, ".png");

   PngIO wrt(0);
//wrt.write(silly, dyn._tbpath);
wrt.write(silly, _scorepath);
*/

return 0;
}
#ifdef __cplusplus
}
#endif

bool format_is_labelless(const char *format) {
    if ((format) && (!strcmp(format,"mat") || !strcmp(format,"mata"))) {
        return 1;
    }
    else {
        return 0;
    }
}

